package br.com.thiagoodev.alcoolgasolina

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.google.android.material.textfield.TextInputEditText

class MainActivity : AppCompatActivity() {
    private lateinit var petrolInput: TextInputEditText
    private lateinit var alcoholInput: TextInputEditText
    private lateinit var resultText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        petrolInput = findViewById(R.id.gasolina_input)
        alcoholInput = findViewById(R.id.alcool_input)
        resultText = findViewById(R.id.result)
    }

    fun handleResult(view: View) {
        val isValid: Boolean = validation()
        if(isValid) {
            val petrolNumber: Double = petrolInput.text?.toString()?.toDouble() ?: 0.0
            val alcoholNumber: Double = alcoholInput.text?.toString()?.toDouble() ?: 0.0

            resultText.text =
                if((alcoholNumber / petrolNumber) >= 0.7) "Melhor utilizar gasolina"
                else "Melhor utilizar alcoól"
        }
    }

    private fun validation(): Boolean {
        resetErrorValidation()

        if((petrolInput.text ?: "").isEmpty()) {
            petrolInput.error = "Você preciosa informar o preço da gasolina"
        }

        if((alcoholInput.text ?: "").isEmpty()) {
            alcoholInput.error = "Você precisa informar o preço do alcoól"
        }

        return (petrolInput.text ?: "").isNotEmpty() || (alcoholInput.text ?: "").isNotEmpty()
    }

    private fun resetErrorValidation() {
        petrolInput.error = null
        alcoholInput.error = null
    }
}